// const connect = require("../db/connect");

// module.exports = class localController {
//   // Função para criar local
//   static async postLocal(req, res) {
//     const { id_local, nome, categoria, localizacao, id_guia } = req.body;

//     // Validar se todos os campos estão sendo preenchidos 
//     if (!id_local || !nome || !categoria || !localizacao || !id_guia) {
//       return res.status(400).json({ erro: "Todos os campos devem ser preenchidos" });
//     };  // Fim do if

//     try {
      
//         // Verifica se já existe uma reserva para o mesmo guia e no mesmo local
//       const overlapQuery = `
//         SELECT * FROM reservas 
//         WHERE id_guia = '${id_guia}' AND id_local = '${id_local}';
//       `;

//       // Insere um novo local com os dados fornecidos
//       const insertQuery = `
//         INSERT INTO local (id_local, nome, categoria, localizacao, id_guia) 
//         VALUES ('${id_local}', '${nome}', ''${categoria}, '${localizacao}', ''${id_guia});
//       `;

//       connect.query(overlapQuery, [id_guia, id_local, new Date()], function (err, result) {

//         const overlapResults = result;

//         if (overlapResults.length > 0) {
//           return res.status(400).json({ erro: "Já existe uma reserva para este guia e local nesta data" });
//         };  // Fim do if

//         connect.query(insertQuery, [id_local, nome, categoria, localizacao, id_guia], function (err, result) {
//           if (err) {
//             return res.status(500).json({ erro: "Erro ao inserir local" });
//           };  // Fim do if

//           console.log("Reserva realizada com sucesso");
//           return res.status(201).json({message: "Reserva realizada com sucesso", valorTotal: valorTotal,
//           });  // Fim do return

//         });  // Fechamento da connect.query

//       });  // Fim do try

//     } catch (error) {
//       console.error("Erro ao executar a consulta:", error);
//       res.status(500).json({ erro: "Erro interno do servidor" });
//     };  // Fechamento do catch
//   };  // Fehamento do postLocal

//   // Função para listar locais
//   static async getAllLocal(req, res) {
//     const query = `SELECT * FROM local;`;

//     connect.query(query, (err, result) => {
//       if (err) {
//         console.log("Erro: " + err);
//         return res.status(500).json({ error: "Erro ao buscar locais" });
//       }

//       if (result.length === 0) {
//         return res.status(404).json({ message: "Locais não encontrados" });
//       }

//       res.status(200).json({ message: "Locais encontrados", locais: result });
//     });
//   }

//   // Função para atualizar local 
//   static async updateLocal(req, res) {
//     const { id_local } = req.params;
//     const { nome, categoria, localizacao, id_guia } = req.body;

//     console.log(id_local, nome, categoria, localizacao, id_guia);

//     if (!id_local || !nome || !categoria || !localizacao || !id_guia) {
//       return res.status(400).json({ erro: "Todos os campos devem ser preenchidos" });
//     };  // Fim do if

//     try {
//         // Consulta para verificar se já existe uma reserva para o mesmo guia e local.
//       const overlapQuery = `
//         SELECT * FROM reservas 
//         WHERE id_guia = '${id_guia}' AND id_local = '${id_local}';
//       `;

//       // Consulta para atualizar as informações de um local específico.
//       const queryUpdate = `
//         UPDATE local 
//         SET nome = '${nome}', categoria = '${categoria}', localizacao = '${localizacao}', id_guia = '${id_guia}' 
//         WHERE id_local = '${id_local}';
//       `;

//       connect.query(overlapQuery, [id_guia, id_local, new Date()], function (err, results) {
//         if (err) {
//           console.log(err);
//           return res.status(500).json({ error: "Erro ao verificar local existente" });
//         };  // Fim do if

//         if (results.length > 0) {
//           return res.status(400).json({ error: "Este local não está disponível para esta data" });
//         };  // Fim do if

//         connect.query(queryUpdate, [nome, categoria, localizacao, id_guia, id_local], function (err, result) {
//           if (err) {
//             console.error("Erro: " + err);
//             return res.status(500).json({ error: "Erro ao atualizar local" });
//           };  // Fim do if

//           if (result.affectedRows === 0) {
//             return res.status(404).json({ message: "Local não encontrado" });
//           };  // Fim do if

//           res.status(200).json({ message: "Local atualizado com sucesso" });
//         });  // Fechamento da connect.query
//       });  // Fim do try
//     } catch (error) {
//       console.error("Erro ao executar a consulta:", error);
//       res.status(500).json({ error: "Erro interno do servidor" });
//     };  // Fechamento do catch
//   };  // Fechamento da updateLocal

//   // Função de deletar local 
//   static async deleteLocal(req, res) {
//     const { id_local } = req.params;
//     const query = `DELETE FROM local WHERE id_local = '${id_local}'`;

//     connect.query(query, [id_local], (err, result) => {
//       if (err) {
//         console.log("Erro: " + err);
//         return res.status(500).json({ error: "Erro ao excluir local" });
//       };  // Fim do if

//       if (result.affectedRows === 0) {
//         return res.status(404).json({ message: "Local não encontrado" });
//       };  // Fim do if

//       res.status(200).json({ message: "Local excluído com sucesso" });
//     });  // Fim da connect.query
//   };  // Fechamento da deleteLocal
// };  // Fechamento da class principal 
