const connect = require("../db/connect");

module.exports = class guiaContoller {

  // Método para criar um novo guia
  static async postGuia(req, res) {
    try {
      const { nome, idiomas, valordoPeriodo, telefone, complemento, email } = req.body;
      let msgerro = "";

      // Validações necessárias 
      if (!nome || !idiomas || !valordoPeriodo || !telefone || !complemento || !email) {  // Dados do corpo da requisição
        msgerro = "Campos inválidos.";  // Validação para conferir se todos os campos são válidos e não estão em branco
      } else if (email && !email.includes("@")) {
        msgerro = "O email deve conter '@'.";  // Validação para o email 
      } else if (!/^[A-ZÀ-Ú]/.test(nome)) {
        msgerro = "O nome do guia deve começar com letra maiúscula.";  // Validação para o nome 
      } else {
        msgerro = "Sucesso!";
      };  // Fim do else

      // Verifica se ocorreu algum erro de validação
      if (msgerro !== "Sucesso!") {
        return res.status(400).json({ message: msgerro });
      } else {
        // Query de inserção no banco
        const query = `INSERT INTO guia (nome, idiomas, valordoPeriodo, telefone, complemento, email) VALUES (?, ?, ?, ?, ?, ?)`;
        const values = [nome, idiomas, valordoPeriodo, telefone, complemento, email];

        // Executa a query de inserção
        connect.query(query, values, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Erro ao cadastrar guia" });
            return;
          };  // Fim do if
          console.log("Inserido no Banco!!!");  // Deu certo
          res.status(201).json({ message: "Guia cadastrado com sucesso." });  // Deu certo
        });  // Fim da connect.query
      };  // Fim do else

    } catch (error) {
      res.status(500).json({ error: error.message });  //  Erro
    };  // Fim do catch
  };  // Fechamento do postGuia

 // Função para atualização do guia logado
static async updateGuia(req, res) {
  const { id_guia } = req.params;
  const { nome, idiomas, valordoPeriodo, telefone, complemento, email } = req.body;

  console.log(id_guia, nome, idiomas, valordoPeriodo, telefone, complemento, email);

  // Validações de entrada
  if (!nome || !idiomas || !valordoPeriodo || !telefone || !complemento || !email) {
    return res.status(400).json({ erro: "Todos os campos devem ser preenchidos" });
  };  // Fim do if 

  if (!email.includes("@")) {
    return res.status(400).json({ erro: "O email deve conter '@'" });  // Validação do email
  };  // Fim do if

  if (!/^[A-ZÀ-Ú]/.test(nome)) {
    return res.status(400).json({ erro: "O nome do guia deve começar com letra maiúscula" });  // nome
  };  // Fim do if

  // Query para atualiza o guia no banco 
  try {
    const queryUpdate = `UPDATE guia SET nome = ?, idiomas = ?, valordoPeriodo = ?, telefone = ?, complemento = ?, email = ? WHERE id_guia = ?`;
    const values = [nome, idiomas, valordoPeriodo, telefone, complemento, email, id_guia];

    // Executa a query de atualização 
    connect.query(queryUpdate, values, function (err, result) {
      if (err) {
        console.error("Erro: " + err);
        return res.status(500).json({ error: "Erro ao atualizar o guia no banco" });  // Deu ruim 
      };  // Fim do if

      if (result.affectedRows === 0) {
        return res.status(404).json({ message: "Guia não encontrado" });  // Não existe esse guia no banco
      }; // Fim do if

      res.status(200).json({ message: "Guia atualizado com sucesso" });  // Deu certo 
    });  // Fim da connect.query
  } catch (error) {
    console.error("Erro ao executar a consulta:", error);
    res.status(500).json({ error: "Erro interno do servidor" });  // Algo deu errado 
  };  // Fim do catch
};  // Fechamento do updateGuia

  // Deletar guia
  static async deleteGuia(req, res) {
    const { id } = req.body;

    // Query para deletar guia no banco 
    const query = `DELETE FROM guia WHERE id_guia = ?`;

    // Executa a query de deleção 
    connect.query(query, [id], function (err) {
      if (err) {
        if (err.code === 'ER_ROW_IS_REFERENCED_2' || err.errno === 1451) {
          return res.status(400).json({ error: "O guia não pode ser deletado pois está associado a uma reserva." });
        } else {
          console.log(err);
          res.status(500).json({ error: "Erro ao deletar o guia no banco!!!" });
        };  // Fim do else
        return;
      };  // Fim do if
      console.log("Deletado do Banco!!!");
      res.status(200).json({ message: "Guia deletado com sucesso." });  // Deu certo 
    });  // Fim da connect.query
  };  // Fechamento do deleteGuia

  // Listar detalhes do guia
  static async getGuia(req, res) {
    const { id } = req.body;

    // Query de listar os guias 
    const query = `SELECT * FROM guia WHERE id_guia = ?`;
    connect.query(query, [id], function (err, results) {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Erro ao buscar detalhes do guia " });
        return;
      };  // Fim do if

      if (results.length === 0) {
        return res.status(404).json({ error: "Guia não encontrado" });
      };  // Fim do if

      return res.status(200).json({ guia: results[0] });
    });  // Fim da connect.query
  };  // Fim do getGuia
};  // Fechamento da  class principal 
