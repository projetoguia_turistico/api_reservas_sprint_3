const connect = require("../db/connect");

module.exports = class reservaController {
  // Função para criação de reserva
  static async postReserva(req, res) {
    const { id_guia, dataInit, dataEnd, periodosReservados, id_usuario} = req.body;

    // Validação inicial dos campos
    if (!id_guia || !dataInit || !dataEnd || !periodosReservados || !id_usuario) {
      return res.status(400).json({ erro: "Todos os campos devem ser preenchidos" });
    };  // Fim do if

    // Verificação e conversão das datas
    const startDate = new Date(dataInit);
    const endDate = new Date(dataEnd);

    if (isNaN(startDate.getTime()) || isNaN(endDate.getTime())) {
      return res.status(400).json({ erro: "Datas inválidas" });
    } // Fechamento do if

    // Verifica se dataInit é menor que dataEnd
    if (startDate >= endDate) {
      return res.status(400).json({ erro: "A data de início deve ser menor que a data de fim" });
    } // Fechamento do if

    //Verificando se o guia já está reservado na data solicitada
    const overlapQuery = `SELECT * FROM reservas WHERE id_guia = '${id_guia}' AND ((dataInit <= '${dataEnd}' AND 
    'dataEnd' >= '${dataInit}'));`;

    try {
      // Verifica se já existe uma reserva para o mesmo guia e no mesmo dia
      console.log("Verificando sobreposição de reservas");

      connect.query(overlapQuery, function (err, result) {
        const overlapResults = result;

        console.log("Resultado da verificação de sobreposição:", overlapResults); // Fim do console

        if (overlapResults.length > 0) {
          return res.status(400).json({ erro: "Este guia não está disponível para esta data" });
        } // Fechamento do if

        // Calculando o número de dias entre dataInit e dataEnd
        const diffTime = Math.abs(endDate - startDate);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        // Consultando o Valor do Período do guia
        console.log("Consultando valor do período do guia");
        const ValordoPeriodoQuery = `SELECT ValordoPeriodo FROM guia WHERE id_guia = '${id_guia}'`;

        connect.query(ValordoPeriodoQuery, function (err, result) {
          const guiaResults = result;
          console.log("Resultado da consulta do valor do período do guia:", guiaResults); // Fim do console

          if (guiaResults.length === 0) {
            return res.status(404).json({ erro: "Guia não encontrado" });  // Guia não existe no banco 
          } // Fechamento do if

          // Extrai o valor do campo ValordoPeriodo do primeiro registro da consulta
          const ValordoPeriodo = guiaResults[0].ValordoPeriodo;

          // Calcular o número total de períodos para cada dia da reserva
          const numPeriodosPorDia = periodosReservados.split(",").length;

          // Calcular o valor total
          const valorTotal = ValordoPeriodo * diffDays * numPeriodosPorDia;

          // Inserindo na tabela de reservas
          console.log("Inserindo reserva na tabela");

          // Query para inserção no banco 
          const insertQuery = `INSERT INTO reservas (id_guia, dataInit, dataEnd, periodosReservados,id_usuario, valorTotal) 
          VALUES ('${id_guia}', '${dataInit}', '${dataEnd}', '${periodosReservados}','${id_usuario}', '${valorTotal}');`;

          connect.query(insertQuery, function (err, result) {

            if (err) {
              return res.status(400).json({ message: "Erro na inserção", err });
            } // Fechamento do if

            // Enviando a resposta com o valorTotal calculado
            console.log("Reserva realizada com sucesso");
            return res.status(201).json({message: "Reserva realizada com sucesso", valorTotal: valorTotal,
            }); // Fechamento da quarta consulta / validação

          }); // Fechamento da terceira consulta / validação

        }); // Fechamento da segunda consulta / validação

      }); // Fechamento da primeira consulta / validação

    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ erro: "Erro interno do servidor" });
    } // Fim do catch
  } //Fechamento postReserva

  // Função para listar reservas
  static async getAllReservas(req, res) {
    const query = `SELECT * FROM reservas;`;

    connect.query(query, (err, result) => {
      if (err) {
        console.log("Erro: " + err);
        return res.status(500).json({ error: "Erro ao buscar reservas" });  // Algo deu errado 
      } // Fechamento do if

      if (result.length === 0) {
        return res.status(404).json({ message: "Reservas não encontradas" });  // Não existe a reserva no banco 
      } // Fehamento do if

      res.status(200).json({ message: "Reservas encontradas", reservas: result });  // Deu certo 
    }); // Fim do connect.query
  } // Fechamento do getAllReservas

  //Função para atualização da reserva
  static async updateReserva(req, res) {
    const { id_reserva } = req.params;
    const { id_guia, dataInit, dataEnd, periodosReservados, id_usuario } = req.body;

    console.log(id_reserva, id_guia, dataInit, dataEnd, periodosReservados, id_usuario);

    // Verifica se todos os campos estão sendo preenchidos 
    if (!id_guia || !dataInit || !dataEnd || !periodosReservados  || !id_usuario) {
      return res.status(400).json({ erro: "Todos os campos devem ser preenchidos" });
    };  // Fim do if

    // Verificando se dataInit é menor que dataEnd
    if (new Date(dataInit) >= new Date(dataEnd)) {
      return res.status(400).json({ erro: "A data de início deve ser menor que a data de fim" });
    };  // Fim do if

    try {
      // Verifica se já existe uma reserva para o mesmo guia e no mesmo dia
      const overlapQuery = `SELECT * FROM reservas
                                    WHERE id_guia = '${id_guia}' AND
                                    (
                                      (dataInit <= '${dataInit}' AND dataEnd >= '${dataEnd}')
                                    )`;

      // Executa a query 
      connect.query(overlapQuery, function (err, results) {

        // Reserva pode não existir no banco 
        if (err) {
          console.log(err);
          return res.status(500).json({ error: "Erro ao verificar reserva existente" });
        };  // Fim do if

        // Guia já está reservado nessa data
        if (results.length > 0) {
          return res.status(400).json({ error: "Este guia não está disponível para esta data",});
        };  // Fim do if

        // Calculando o número de dias entre dataInit e dataEnd
        const diffTime = Math.abs(new Date(dataEnd) - new Date(dataInit));
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        // Consultando o ValordoPeriodo do guia
        const ValordoPeriodoQuery = `SELECT ValordoPeriodo FROM guia WHERE id_guia = '${id_guia}'`;

        // Executa a query 
        connect.query(ValordoPeriodoQuery, function (err, results) {
          if (err) {
            return res.status(500).json({ error: "Erro ao consultar o valor do guia" });  // Algo deu errado 
          };  // Fim do if

          if (results.length === 0) {
            return res.status(404).json({ error: "guia não encontrado" });  // Guia não existe no banco 
          };  // Fim do if

          // Extrai o valor do campo ValordoPeriodo do primeiro registro da consulta
          const ValordoPeriodo = results[0].ValordoPeriodo;

          // Calcular o número total de períodos para cada dia da reserva
          const numPeriodosPorDia = periodosReservados.split(",").length;

          // Calcular o valor total
          const valorTotal = ValordoPeriodo * diffDays * numPeriodosPorDia;

          // Query para atualizar reserva já existente no banco 
          const queryUpdate = `UPDATE reservas SET id_guia = ?, dataInit = ?, dataEnd = ?, periodosReservados = ?, id_usuario = ?, valorTotal = ? WHERE id_reserva = ?`;

          // Executa a query de atualização 
          connect.query(queryUpdate, [id_guia, dataInit, dataEnd, periodosReservados, id_usuario, valorTotal, id_reserva], function (err, result) {
            if (err) {
              console.error("Erro: " + err);
              return res.status(500).json({ error: "Erro ao atualizar sua reserva" });  // Deu ruim 
            };  // Fim do if

            if (result.affectedRows === 0) {
              return res.status(404).json({ message: "Reserva não encontrada" });  // Reserva não existe no banco 
            }; // Fim do if

            res.status(200).json({ message: "Reserva atualizada com sucesso", "Valor Total": valorTotal,});
          });  // Fim do connect.query

        });  // Fim do connect.query

      });  // Fim do connect.query

    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    };  // Fim do catch
  };  // Fechamento do updateReserva

  //Função para deletar a reserva
  static async deleteReserva(req, res) {
    const { id_reserva } = req.params;

    // Query de deleção de reserva por id da reserva
    const query = `DELETE FROM reservas WHERE id_reserva = '${id_reserva}'`;

    // Executa a query de deleção 
    connect.query(query, (err, result) => {
      if (err) {
        console.log("Erro: " + err);
        return res.status(500).json({ error: "Erro ao excluir reserva" });  // Algo deu errado 
      };  // Fim do if

      if (result.affectedRows === 0) {
        return res.status(404).json({ message: "Reserva não encontrada" });  // Reserva não existe no banco 
      };  // Fim do if

      res.status(200).json({ message: "Reserva excluída com sucesso" });  // Deu certo 
    });  // Fim do connect.query
  }; // Fim da deleteReserva
};  // Fechamento da class principal