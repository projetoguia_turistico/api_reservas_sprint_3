const connect = require("../db/connect");

module.exports = class userController {
  // Criar um novo usuário
  static async postUser(req, res) {
    try {
      const { nome, senha, email, telefone, confirmarSenha } = req.body;
      let msgerro = "";

      // Validações
      if (!email.includes("@")) { msgerro = "O email deve conter '@'.";} 
        else if (senha.length < 4) { msgerro = "A senha deve ter no mínimo 4 caracteres.";} 
        else if (!/^[A-ZÀ-Ú]/.test(nome)) { msgerro = "O nome do usuário deve começar com letra maiúscula.";} 
        else if ( nome === "" || senha === "" || email === "" || telefone === "" ) { msgerro = "Campos inválidos.";} 
        else if (senha !== confirmarSenha) { msgerro = "As senhas não coincidem."; }
        else { msgerro = "Sucesso!";}

      console.log("Email", email);

      if (msgerro !== "Sucesso!") {
        return res.status(400).json({ message: msgerro });
      } else {
        // Query de inserção no banco 
        const query = `INSERT INTO usuario (nome, senha, email, telefone) VALUES ('${nome}', '${senha}', '${email}', '${telefone}')`;

        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Erro ao cadastrar usuário" });
            return;
          };  // Fim do if
          console.log("Inserido no Banco!!!");
          res.status(201).json({ message: "Usuário cadastrado com sucesso." });
        });  // Fim do connect.query
      };  // Fim do else
    } catch (error) {
      res.status(500).json({ error: error.message });
    };  // Fim do catch
  };  // Fechamento do postUser

  // Editar o usuário logado
  static async editarUser(req, res) {
    const { id_usuario } = req.params;
    const { nome, senha, email, telefone, confirmarSenha } = req.body;
    let msgerro = "";

    // Validações necessárias 
    if (!email.includes("@")) { msgerro = "O email deve conter '@'."; }
    else if (senha.length < 4) { msgerro = "A senha deve ter no mínimo 4 caracteres.";}
    else if (!/^[A-ZÀ-Ú]/.test(nome)) { msgerro = "O nome do usuário deve começar com letra maiúscula.";}
    else if (nome === "" || senha === "" || email === "" || telefone === "") { msgerro = "Campos inválidos.";} 
    else if (senha !== confirmarSenha) { msgerro = "As senhas não coincidem.";}

    if (msgerro) {
      return res.status(400).json({ error: msgerro });
    } else {
      // Query para atualização do user 
      const query = `UPDATE usuario SET nome = ?, senha = ?, email = ?, telefone = ? WHERE id_usuario = ?`;
      const values = [nome, senha, email, telefone, id_usuario];

      // Executa a query de atualização 
      connect.query(query, values, function (err) {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: "Erro ao atualizar o usuário no banco!!!" });  // Algo deu errado 
        };  // Fim do if

        console.log("Atualizado no Banco!!!");
        return res.status(200).json({ message: "Usuário atualizado com sucesso." });  // Deu certo 
      });  // Fim da connect.query
    };  // Fechamento do else
  };  // Fechamento do editarUser

  // Deletar o usuário logado
  static async deleteUser(req, res) {
    const { id } = req.body;

    // Query de deleção no banco 
    const query = `DELETE FROM usuario WHERE id_usuario = ${id}`;

    // Executa a query de deleção 
    connect.query(query, function (err) {
      if (err) {
        console.log(err);
        return res.status(500).json({ error: "Erro ao deletar o usuário no banco!!!" });  // Algo deu errado 
      };  // Fim do if 

      console.log("Deletado do Banco!!!");
      return res.status(200).json({ message: "Usuário deletado com sucesso." });  // Deu certo 
    });  // Fim do connect.query
  };  // Fechamento deleteUser

  // Buscar os detalhes do usuário 
  static async getUserDetails(req, res) {
    const { id } = req.body;

    // Query para listar usuários
    const query = `SELECT * FROM usuario WHERE id_usuario = ${id}`;

    // Executa a query de listagem dos usuários
    connect.query(query, function (err, results) {
      if (err) {
        console.log(err);
        return res.status(500).json({ error: "Erro ao buscar detalhes do usuário" });  // Algo deu errado 
      };  // Fim do if

      if (results.length === 0) {
        return res.status(404).json({ error: "Usuário não encontrado" });  // O usuário pode não existir no banco 
      };  // Fim do if

      return res.status(200).json({ usuario : results[0] });
    });  // Fim do connect.query
  };  // Fechamento do getUserDetails

  //Fazer login
  static async postLogin(req, res) {
    const { email, senha } = req.body;

    if (!email || !senha) {
      return res.status(400).json({ error: "Email e senha são obrigatórios" });  // Email e senha não foram preeenchidos
    };  // Fim do if

    // Query para fazer login 
    const query = `SELECT * FROM usuario WHERE email = '${email}' AND senha = '${senha}'`;

    try {
      // Executa a query de fazer login 
      connect.query(query, function (err, results) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });  // Algo deu errado 
        };  // Fim do if

        if (results.length === 0) {
          return res.status(401).json({ error: "Credenciais inválidas" });  // Algo de errado nos campos 
        };  // Fim do if

        return res.status(200).json({ message: "Login realizado com sucesso", user: results[0] });  // Deu certo 
      });  // Fechamento do try

    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });  // ALgo está errado 
    };  // Fim do catch
  };  // Fechamento do postLogin
};  // Fechamento da class principal 