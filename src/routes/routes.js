const router = require('express').Router()
const userController = require('../controller/userController');
const guiaController = require('../controller/guiaContoller');
const dbController = require('../controller/dbController');
const reservaController = require('../controller/reservaController');
// const localController = require('../controller/localController');


// rotas para user
router.post('/postUser/', userController.postUser);  // rota para criar usuário
router.post('/user/', userController.postUser);
router.post('/login/', userController.postLogin);
router.get('/user/', userController.getUserDetails);  // rota para listar usuários
router.put('/editaruser/:id_usuario', userController.editarUser);  // rota para atualizar usuário
router.delete('/user/', userController.deleteUser);  // rota para deletar usuário



// rotas para o guia 
router.post('/guia/', guiaController.postGuia);  // rota para criar guia 
router.get('/guia/', guiaController.getGuia);  // rota para listar guia
router.put('/editarguia/:id_guia', guiaController.updateGuia);  // rota para atualizar guia
router.delete('/guia/', guiaController.deleteGuia);  // rota para deletar guia

// rota para vizualização dos nomes e descrições das tabelas.
router.get("/tables", dbController.getTables);  // rota para exibir os nomes das tabelas.
router.get("/tablesDescription", dbController.getTablesDescriptions); // rota para exibir as descrições das tabelas.

// rotas para reservas
router.post("/criarReserva", reservaController.postReserva);  // rota para criar uma reserva
router.get("/listarReservas", reservaController.getAllReservas);  // rota para listar reserva
router.put("/atualizarReserva/:id_reserva", reservaController.updateReserva);  // rota para atualizar reserva
router.delete("/deletarReserva/:id_reserva", reservaController.deleteReserva);  // rota para deletar reserva

// // rotas para local 
// router.post("/criarLocal/", localController.postLocal);
// router.get("/listarLocal", localController.getAllLocal);
// router.put("/atualizarLocal", localController.updateLocal);
// router.delete("/excluirLocal", localController.deleteLocal);

module.exports = router;