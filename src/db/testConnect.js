const connect = require ("./connect");

module.exports = function testConnect(){
    try{
    const query = `SELECT 'Conexão bem-sucedida' AS Mensagem`;
    connect.query(query, function(err){
        if(err){
            console.log("Erro na conexão: " + err)
            return;
        }
        console.log("Conexão Realizada com Mysql!");
    })
//função de callback é uma função de retorno
}catch(error){
    console.log("Erro ao executar consulta:", error);
}
};